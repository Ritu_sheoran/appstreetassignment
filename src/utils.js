export function isEqual(array1, array2) {
  if (array1.length !== array2.length) {
    return false;
  } else {
    let isEqual = true;
    array1.forEach((element, index) => {
      if (element !== array2[index] && isEqual === true) {
        isEqual = false;
      }
    });
    return isEqual;
  }
}

export const colorCodes = {
  'Rose Gold': '#b76e79',
  'Space Grey': '#343d46',
  Gold: 'Gold',
  Black: 'Black',
  Silver: 'Silver',
  'Black Grey': '#C0C0C0'
};
