import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Route, Redirect } from 'react-router-dom';
import ProductList from './components/product/productList/ProductList';
import ProductDetail from './components/product/productDetail/ProductDetail';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Route path="/" exact component={ProductList} />
        <Route path="/product-detail/:productId" component={ProductDetail} />
      </div>
    );
  }
}

export default App;
