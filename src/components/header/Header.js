import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Header.css';

export default class Header extends Component {
  render() {
    return (
      <div className="headerParent">
        <div className="header">
          <Link to="/products">
            <div className="headerLeft">
              <h4>MY AWESOME SHOP</h4>
            </div>
          </Link>
          <div className="headerRight">
            <div className="headerRightButton">
              <h4>HOME</h4>
            </div>
            <div className="headerRightButton">
              <h4>ABOUT</h4>
            </div>
            <div className="headerRightButton">
              <h4>CONTACT</h4>
            </div>
            <div className="headerRightButton">
              <h4>BAG</h4>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
