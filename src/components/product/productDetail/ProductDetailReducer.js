export default function ProductDetailReducer(state = {}, action) {
  switch (action.type) {
    case 'SAVE_PRODUCT_DETAIL':
      return {
        ...state,
        ...action.productDetail
      };
    case 'SAVE_SELECTED_OPTION':
      return {
        ...state,
        selectedOption: [...action.selectedOption]
      };
    default: {
      return { ...state };
    }
  }
}
