import pathOr from 'lodash/fp/pathOr';
import { SAVE_PRODUCT_DETAIL, SAVE_SELECTED_OPTION } from './constants';
export function getProductDetails(productId) {
  // console.log("productId >>>>>>>",productId);
  return dispatch => {
    let options = {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    };
    fetch(
      `https://assignment-appstreet.herokuapp.com/api/v1/products/${productId}`,
      options
    )
      .then(res => res.json())
      .catch(err => console.log(err))
      .then(product => {
        // console.log("product detail >>>>>>>>.",JSON.stringify(product))
        dispatch(saveProductDetail(product));
        const selectedOption = pathOr([], 'selected_option_ids', product);
        dispatch(saveSelectedOption(selectedOption));
      });
  };
}

function saveProductDetail(product) {
  return { type: SAVE_PRODUCT_DETAIL, productDetail: { ...product } };
}

export function saveSelectedOption(option) {
  return { type: SAVE_SELECTED_OPTION, selectedOption: option };
}
