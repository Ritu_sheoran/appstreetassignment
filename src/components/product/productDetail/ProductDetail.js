import React, { Component } from 'react';
import { connect } from 'react-redux';
import pathOr from 'lodash/fp/pathOr';
import './ProductDetail.css';
import { getProductDetails, saveSelectedOption } from './ProductDetailAction';
import { isEqual, colorCodes } from '../../../utils';
class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.changeSelectedAttribute = this.changeSelectedAttribute.bind(this);
  }

  componentDidMount() {
    const { productId } = pathOr(null, 'match.params', this.props);
    if (productId) {
      this.props.dispatch(getProductDetails(productId));
    }
  }

  changeSelectedAttribute(attributeOption, value) {
    const { dispatch, attributesOptions, selectedOption } = this.props;
    let [selectedAttributesList = []] = attributesOptions.filter(
      element => element.length && element[0].attriName === value.attriName
    );
    let ids = selectedAttributesList.map(element => element._id);
    let newSelectedOption = selectedOption.map(element => {
      if (ids.indexOf(element) > -1) {
        return value._id;
      }

      return element;
    });
    dispatch(saveSelectedOption(newSelectedOption));
  }

  renderProductDetails(finalData, productVariation, attributesOptions) {
    return (
      <div className="productDetailRight">
        <div className="productDetailTitle">
          <h4>{finalData.name}</h4>
        </div>
        <div className="productDetailTitleDescription">{finalData.desc}</div>
        <hr className="hrStyle" />
        <div className="productDetailTitle">
          <span className="productDetailPrice">
            <b
              style={
                finalData.salePrice
                  ? {
                      color: 'red',
                      textDecoration: 'line-through'
                    }
                  : {}
              }
            >
              Rs.{finalData.markPrice && finalData.markPrice.toFixed(2)}
            </b>
          </span>
          <b>Rs.{finalData.salePrice && finalData.salePrice.toFixed(2)}</b>
          <br />
          {finalData.salePrice && (
            <div className="productofferDiv">
              <b>
                You Save Rs.{(
                  finalData.markPrice - finalData.salePrice
                ).toFixed(2)}
              </b>
            </div>
          )}
          <div className="productText">
            Local taxes included (where applicable)
          </div>
        </div>
        <hr className="hrStyle" />
        <div className="productText" />
        {this.renderProductAttribures(attributesOptions)}
        <br />
        <div className="productDetailTitle">
          <b>Quantity</b>
          <br />
          <br />
          <input
            type="number"
            name="quantity"
            min="1"
            max="10"
            defaultValue={1}
          />
        </div>
        <div>
          <button><b>Add To cart</b></button>
        </div>
      </div>
    );
  }

  renderProductAttribures(attributesOptions) {
    const { selectedOption = [] } = this.props;
    return attributesOptions.map((attributeOption, docIndex) => {
      let data = attributeOption.map((val, index) => {
        const isColorElem = val.attriName === 'Colour';
        return (
          <div key={index} className="productDetailattributeName">
            <div
              className={`productDetailRadio ${selectedOption.indexOf(val._id) >
                -1 && 'selected'}`}
              onClick={this.changeSelectedAttribute.bind(
                this,
                attributeOption,
                val
              )}
            >
              {isColorElem && (
                <div
                  className="colorDemo"
                  style={{ backgroundColor: colorCodes[val.name] }}
                />
              )}
              <span style={isColorElem ? { marginLeft: 10 } : {}}>
                {val.name}
              </span>
            </div>
          </div>
        );
      });
      return (
        <div key={docIndex} className="productDetailattribute">
          <b>
          {attributeOption.length + ' ' + attributeOption[0].attriName}{' '}
            available </b><div className="attributeWrapper">{data}</div>
          <br />
        </div>
      );
    });
  }

  render() {
    const { finalData, productVariation, attributesOptions } = this.props;
    return (
      <div className="productDetail">
        <div className="productDetailLeft">
          <div className="productDetailLeftImg">
            <img src={finalData.image} />{' '}
          </div>
        </div>
        {this.renderProductDetails(
          finalData,
          productVariation,
          attributesOptions
        )}
      </div>
    );
  }
}

var mapStateToProps = state => {
  if (state && state.ProductDetailReducer && state.ProductDetailReducer) {
    let finalData = {};
    const {
      attributes = [],
      selectedOption = [],
      primary_product: primaryProduct = {},
      options = []
    } = state.ProductDetailReducer;

    if (primaryProduct.desc) {
      finalData['desc'] = primaryProduct.desc;
    }

    let attributesOptions = [];
    for (var i in attributes) {
      let filteredOptions = options.filter(option => {
        if (attributes[i]._id === option.attrib_id) {
          option['attriName'] = attributes[i].name;
          return option;
        }
        return false;
      });
      attributesOptions.push(filteredOptions);
    }

    let productVariation = pathOr(
      [],
      'product_variations',
      state.ProductDetailReducer
    );
    if (productVariation.length && selectedOption.length) {
      for (let i in productVariation) {
        let productSign = productVariation[i].sign.sort();
        let selected = selectedOption.sort();
        if (isEqual(productSign, selected)) {
          productVariation = productVariation[i];
          break;
        }
      }
      finalData['name'] = productVariation.name || '';
      finalData['markPrice'] = productVariation.mark_price || '';
      finalData['salePrice'] = productVariation.sale_price || '';
      finalData['image'] =
        productVariation.images && productVariation.images.length
          ? productVariation.images[0]
          : '';
    }
    return { finalData, productVariation, attributesOptions, selectedOption };
  }
};

ProductDetail = connect(mapStateToProps)(ProductDetail);

export default ProductDetail;
