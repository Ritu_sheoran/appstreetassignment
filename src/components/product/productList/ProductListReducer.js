export default function ProductListReducer(state = {}, action) {
  switch (action.type) {
    case 'SAVE_PRODUCTS':
      return { ...state, ...action.products };
    default:
      return { ...state };
  }
}
