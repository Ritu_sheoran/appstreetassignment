import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import Header from "../src/components/header/Header";
import Footer from "../src/components/footer/Footer";
import ReducerExport from "./components/CombineReducers";
import registerServiceWorker from "./registerServiceWorker";
import { BrowserRouter as Router } from "react-router-dom";
import {Provider} from"react-redux";
import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";

let store = createStore(combineReducers(ReducerExport), applyMiddleware(thunk));

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div className="mainDiv">
        <Header />
        <App />
        <Footer />
      </div>
    </Router>
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
